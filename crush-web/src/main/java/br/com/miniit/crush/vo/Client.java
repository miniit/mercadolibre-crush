package br.com.miniit.crush.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Client implements Serializable {

	private static final long serialVersionUID = -1223922044734800813L;
	private String id;
	@Size(min = 3, max = 100)
	private String name;
	@Size(min = 3, max = 50)
	private String nickname;
	@Size(min = 3, max = 100)
	@Pattern(regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\." + "[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
			+ "(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
	private String email;
	@Pattern(regexp = "^\\(?(\\d{2})\\)?[- ]?(\\d{5})[- ]?(\\d{4})$")
	private String phone;
	@Size(min = 3, max = 100)
	private String city;
	private Map<Integer, String> ufs;
	private int selectedUf;
	@NotNull
	private Date registerDate;
	private List<Order> orders;

	public Client() {
		orders = new ArrayList<>();
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * @param nickname
	 *            the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the ufs
	 */
	public Map<Integer, String> getUfs() {
		return ufs;
	}

	/**
	 * @param ufs
	 *            the ufs to set
	 */
	public void setUfs(Map<Integer, String> ufs) {
		this.ufs = ufs;
	}

	/**
	 * @return the selectedUf
	 */
	public int getSelectedUf() {
		return selectedUf;
	}

	/**
	 * @param selectedUf
	 *            the selectedUf to set
	 */
	public void setSelectedUf(int selectedUf) {
		this.selectedUf = selectedUf;
	}

	/**
	 * @return the registerDate
	 */
	public Date getRegisterDate() {
		return registerDate;
	}

	/**
	 * @param registerDate
	 *            the registerDate to set
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * @return the orders
	 */
	public List<Order> getOrders() {
		return orders;
	}

	/**
	 * @param orders the orders to set
	 */
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
}
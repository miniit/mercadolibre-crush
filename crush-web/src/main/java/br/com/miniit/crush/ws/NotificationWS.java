package br.com.miniit.crush.ws;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import br.com.miniit.crush.vo.Notification;

/**
 * Exposes the RESTful service to ML notifications.
 * 
 * More information:
 * {@link http://developers.mercadolibre.com/pt-br/gerenciamento-de-vendas/#Receber-Notifica%C3%A7%C3%A3o}
 */
@Stateless
@LocalBean
public class NotificationWS {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(NotificationWS.class);

	/**
	 * Receive a notification from ML.
	 * 
	 * @param notification
	 * @return HTTP 200 OK
	 */
	@POST
	@Path("/post")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createProductInJSON(Notification notification) {

		if (logger.isDebugEnabled()) {
			logger.debug(notification.toString());
		}

		return Response.status(Status.OK).build();
	}

}

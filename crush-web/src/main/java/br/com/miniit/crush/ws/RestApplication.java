package br.com.miniit.crush.ws;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("notification")
public class RestApplication extends Application {
}

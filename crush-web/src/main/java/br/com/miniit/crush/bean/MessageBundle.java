package br.com.miniit.crush.bean;
import java.io.Serializable;
import java.util.PropertyResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Named;
 
@Named
public class MessageBundle implements Serializable {
     
	private static final long serialVersionUID = -7396431084392267872L;
	private PropertyResourceBundle msg;

	@PostConstruct
    public void init() {
		FacesContext context = FacesContext.getCurrentInstance();
		msg = context.getApplication().evaluateExpressionGet(context, "#{msg}", PropertyResourceBundle.class);
    }
	
	public String getString(String key) {
		return msg.getString(key);
	}
}
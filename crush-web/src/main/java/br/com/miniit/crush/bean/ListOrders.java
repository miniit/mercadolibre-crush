package br.com.miniit.crush.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;

import br.com.miniit.crush.vo.Client;
import br.com.miniit.crush.vo.Order;
import br.com.miniit.crush.vo.Product;

@Named
@ViewScoped
public class ListOrders implements Serializable {

	private static final long serialVersionUID = -5451701575191675361L;
	@Inject
	private MessageBundle msg;
	private List<Order> orders;
	private String contentHeader;
	private int searchCriteria;
	private String searchText;

	@PostConstruct
	public void init() {
		contentHeader = msg.getString("listOrders");
		orders = new ArrayList<>();
		Order order = null;
		for (int i = 1; i <= 11; i++) {
			order = new Order();
			order.setNumber(i);
			Client client = new Client();
			client.setName("Rafael Or�gio");
			order.setClient(client);
			List<Product> products = new ArrayList<>();
			Product product = null;
			for (int j = 1; j <= 11; j++) {
				product = new Product();
				product.setId(String.valueOf(j));
				product.setName("Call of Duty " + (j));
				product.setValue(new BigDecimal(100.00 * i));
				product.setQuantity(i);
				products.add(product);
			}
			order.setProducts(products);
			orders.add(order);
		}
	}

	public void edit(Order order) {
		System.out.println(order);
	}

	public void delete(Order order) {
		orders.remove(order);
	}

	public void search() {
		
	}
	
	/**
	 * @return the orders
	 */
	public List<Order> getOrders() {
		return orders;
	}

	/**
	 * @param orders
	 *            the orders to set
	 */
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	/**
	 * @return the contentHeader
	 */
	public String getContentHeader() {
		return contentHeader;
	}

	/**
	 * @param contentHeader
	 *            the contentHeader to set
	 */
	public void setContentHeader(String contentHeader) {
		this.contentHeader = contentHeader;
	}

	/**
	 * @return the searchCriteria
	 */
	public int getSearchCriteria() {
		return searchCriteria;
	}

	/**
	 * @param searchCriteria
	 *            the searchCriteria to set
	 */
	public void setSearchCriteria(int searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	/**
	 * @return the searchText
	 */
	public String getSearchText() {
		return searchText;
	}

	/**
	 * @param searchText
	 *            the searchText to set
	 */
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
}
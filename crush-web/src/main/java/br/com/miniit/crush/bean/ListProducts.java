package br.com.miniit.crush.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;

import br.com.miniit.crush.vo.Product;

@Named
@ViewScoped
public class ListProducts implements Serializable {

	private static final long serialVersionUID = -5451701575191675361L;
	@Inject
	private MessageBundle msg;
	private List<Product> products;
	private String contentHeader;
	private int searchCriteria;
	private String searchText;

	@PostConstruct
	public void init() {
		contentHeader = msg.getString("listProducts");
		products = new ArrayList<>();
		Product product = null;
		Map<Integer, String> plataforms = new LinkedHashMap<>();
		plataforms.put(1, "Nintendo");
		plataforms.put(2, "X-box One");
		Map<Integer, String> categories = new LinkedHashMap<>();
		categories.put(1, "Cash");
		categories.put(2, "Level-up");
		Map<Integer, String> countries = new LinkedHashMap<>();
		countries.put(1, "Brasil");
		countries.put(2, "Alemanha");
		for (int i = 1; i <= 11; i++) {
			product = new Product();
			product.setName("Call of Duty " + i);
			product.setPlataforms(plataforms);
			product.setSelectedPlataform(1);
			product.setCategories(categories);
			product.setSelectedCategory(2);
			product.setCountries(countries);
			product.setSelectedCountry(1);
			products.add(product);
		}
	}

	public void edit(Product product) {
		System.out.println(product);
	}

	public void delete(Product product) {
		products.remove(product);
	}

	public void search() {
		
	}
	
	/**
	 * @return the products
	 */
	public List<Product> getProducts() {
		return products;
	}

	/**
	 * @param products
	 *            the products to set
	 */
	public void setProducts(List<Product> products) {
		this.products = products;
	}

	/**
	 * @param products
	 *            the products to set
	 */
	public void setOrders(List<Product> orders) {
		this.products = orders;
	}

	/**
	 * @return the contentHeader
	 */
	public String getContentHeader() {
		return contentHeader;
	}

	/**
	 * @param contentHeader
	 *            the contentHeader to set
	 */
	public void setContentHeader(String contentHeader) {
		this.contentHeader = contentHeader;
	}

	/**
	 * @return the searchCriteria
	 */
	public int getSearchCriteria() {
		return searchCriteria;
	}

	/**
	 * @param searchCriteria
	 *            the searchCriteria to set
	 */
	public void setSearchCriteria(int searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	/**
	 * @return the searchText
	 */
	public String getSearchText() {
		return searchText;
	}

	/**
	 * @param searchText
	 *            the searchText to set
	 */
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
}
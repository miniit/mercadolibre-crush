package br.com.miniit.crush.bean;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
 
@Named
@ViewScoped
public class Product implements Serializable {
     
	private static final long serialVersionUID = -8911922912307355596L;
	@Inject
	private MessageBundle msg;
	private br.com.miniit.crush.vo.Product product;
	private String contentHeader;
    
	@PostConstruct
    public void init() {
		contentHeader = msg.getString("enterProduct");
		product = new br.com.miniit.crush.vo.Product();
		Map<Integer, String> plataforms = new LinkedHashMap<>();
		plataforms.put(1, "Nintendo");
		plataforms.put(2, "X-box One");
		plataforms.put(3, "Playstation 3");
		plataforms.put(4, "Playstation 4");
		plataforms.put(5, "PC");
		plataforms.put(6, "Google Play");
		plataforms.put(7, "iTunes");
		Map<Integer, String> categories = new LinkedHashMap<>();
		categories.put(1, "Cash");
		categories.put(2, "Level-up");
		categories.put(3, "C�digo");
		categories.put(4, "Conta de jogo");
		Map<Integer, String> countries = new LinkedHashMap<>();
		countries.put(1, "Brasil");
		countries.put(2, "Alemanha");
		countries.put(3, "Estados Unidos");
		countries.put(4, "Jap�o");
		product.setPlataforms(plataforms);
		product.setCategories(categories);
		product.setCountries(countries);
    }
	
	public void save(ActionEvent event) {
	}

	/**
	 * @return the product
	 */
	public br.com.miniit.crush.vo.Product getProduct() {
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(br.com.miniit.crush.vo.Product product) {
		this.product = product;
	}

	/**
	 * @return the contentHeader
	 */
	public String getContentHeader() {
		return contentHeader;
	}

	/**
	 * @param contentHeader the contentHeader to set
	 */
	public void setContentHeader(String contentHeader) {
		this.contentHeader = contentHeader;
	}
}
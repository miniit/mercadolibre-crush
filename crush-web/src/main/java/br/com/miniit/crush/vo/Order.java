package br.com.miniit.crush.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order implements Serializable {

	private static final long serialVersionUID = -7100743005877395028L;
	private long number;
	private Client client;
	private List<Product> products;
	private Date date;

	public Order() {
		client = new Client();
		products = new ArrayList<>();
	}

	public int getOrderQuantity() {
		int quantity = 0;
		for (Product product : products) {
			quantity += product.getQuantity();
		}
		return quantity;
	}

	public BigDecimal getOrderValue() {
		BigDecimal orderValue = new BigDecimal(0);
		for (Product product : products) {
			orderValue = orderValue.add(product.getTotalValue());
		}
		return orderValue;
	}

	/**
	 * @return the number
	 */
	public long getNumber() {
		return number;
	}

	/**
	 * @param number
	 *            the number to set
	 */
	public void setNumber(long number) {
		this.number = number;
	}

	/**
	 * @return the client
	 */
	public Client getClient() {
		return client;
	}

	/**
	 * @param client
	 *            the client to set
	 */
	public void setClient(Client client) {
		this.client = client;
	}

	/**
	 * @return the products
	 */
	public List<Product> getProducts() {
		return products;
	}

	/**
	 * @param products
	 *            the products to set
	 */
	public void setProducts(List<Product> products) {
		this.products = products;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
}
package br.com.miniit.crush.bean;

import java.awt.event.ActionEvent;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
 
@Named
@ViewScoped
public class Client implements Serializable {
     
	private static final long serialVersionUID = -3802743458641302173L;
	@Inject
	private MessageBundle msg;
	private br.com.miniit.crush.vo.Client client;
	private String contentHeader;
    
	@PostConstruct
    public void init() {
		contentHeader = msg.getString("enterClient");
		client = new br.com.miniit.crush.vo.Client();
    }
	
	public void save(ActionEvent event) {
	}

	/**
	 * @return the client
	 */
	public br.com.miniit.crush.vo.Client getClient() {
		return client;
	}

	/**
	 * @param client the client to set
	 */
	public void setClient(br.com.miniit.crush.vo.Client client) {
		this.client = client;
	}

	/**
	 * @return the contentHeader
	 */
	public String getContentHeader() {
		return contentHeader;
	}

	/**
	 * @param contentHeader the contentHeader to set
	 */
	public void setContentHeader(String contentHeader) {
		this.contentHeader = contentHeader;
	}
}
package br.com.miniit.crush.bean;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.omnifaces.cdi.ViewScoped;
import org.primefaces.event.SelectEvent;

import br.com.miniit.crush.vo.Product;
 
@Named
@ViewScoped
public class Order implements Serializable {
     
	private static final long serialVersionUID = -8911922912307355596L;
	@Inject
	private MessageBundle msg;
	private br.com.miniit.crush.vo.Order order;
	private List<Product> selectedProducts;
	private String productQuery;
	private String clientQuery;
	private String contentHeader;
    
	@PostConstruct
    public void init() {
		contentHeader = msg.getString("enterOrder");
		order = new br.com.miniit.crush.vo.Order();
		Product product = null;
		for(int i = 1; i <= 11; i++) {
			product = new Product();
			product.setId(String.valueOf(i));
			product.setName("Call of Duty " + (i));
			product.setValue(new BigDecimal(100.00 * i));
			product.setQuantity(i);
			order.getProducts().add(product);
		}
    }
	
	public List<String> onProductCompleteText(String query) {
        List<String> results = new ArrayList<String>();
        for(int i = 0; i < 10; i++) {
            results.add(query + i);
        }
        return results;
    }
	
	public void onProductSelect(SelectEvent event) {
		Product product = new Product();
		product.setId(event.getObject().toString());
		product.setName("Call of Duty 123");
		product.setValue(new BigDecimal(new Random().nextDouble() * (new Random().nextDouble() * 10)));
		product.setQuantity(3);
		order.getProducts().add(product);
		productQuery = StringUtils.EMPTY;
    }
	
	public List<String> onClientCompleteText(String query) {
        List<String> results = new ArrayList<String>();
        for(int i = 0; i < 10; i++) {
            results.add("Rafael Or�gio " + query + i);
        }
        return results;
    }
	
	public void onClientSelect(SelectEvent event) {
		
		order.getClient().setId(event.getObject().toString());
		order.getClient().setName(event.getObject().toString());
		order.getClient().setEmail("rafael.oragio@gmail.com");
		order.getClient().setNickname("Rafa");
		order.getClient().setPhone("(19) 9953-5827");
		clientQuery = StringUtils.EMPTY;
    }
	
	public void deleteProducts(ActionEvent event) {
		for(Product product : selectedProducts) {
			order.getProducts().remove(product);
		}
	}
	
	public BigDecimal getOrderValue() {
		BigDecimal orderValue = new BigDecimal(0);
		for(Product product : order.getProducts()) {
			orderValue = orderValue.add(product.getTotalValue());
		}
		return orderValue;
	}
	
	public void save(ActionEvent event) {
	}
	
	/**
	 * @return the order
	 */
	public br.com.miniit.crush.vo.Order getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(br.com.miniit.crush.vo.Order order) {
		this.order = order;
	}

	/**
	 * @return the selectedProducts
	 */
	public List<Product> getSelectedProducts() {
		return selectedProducts;
	}

	/**
	 * @param selectedProducts the selectedProducts to set
	 */
	public void setSelectedProducts(List<Product> selectedProducts) {
		this.selectedProducts = selectedProducts;
	}

	/**
	 * @return the productQuery
	 */
	public String getProductQuery() {
		return productQuery;
	}

	/**
	 * @param productQuery the productQuery to set
	 */
	public void setProductQuery(String productQuery) {
		this.productQuery = productQuery;
	}
	
	/**
	 * @return the clientQuery
	 */
	public String getClientQuery() {
		return clientQuery;
	}

	/**
	 * @param clientQuery the clientQuery to set
	 */
	public void setClientQuery(String clientQuery) {
		this.clientQuery = clientQuery;
	}
	
	/**
	 * @return the contentHeader
	 */
	public String getContentHeader() {
		return contentHeader;
	}

	/**
	 * @param contentHeader the contentHeader to set
	 */
	public void setContentHeader(String contentHeader) {
		this.contentHeader = contentHeader;
	}
}
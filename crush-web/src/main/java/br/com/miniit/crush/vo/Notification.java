package br.com.miniit.crush.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Special VO for receiving ML Notifications.
 * 
 * Example of a notification:
 * 
 * <pre>
 * {
 *     "user_id": 1234,
 *     "resource": "/orders/731867397",
 *     "topic": "orders",
 *     "received": "2011-10-19T16:38:34.425Z",
 *     "application_id": 14529,
 *     "sent": "2011-10-19T16:40:34.425Z",
 *     "attempts" : 0
 * }
 * </pre>
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Notification implements Serializable {

	/**
	 * serial uid.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * json: user_id
	 */
	@JsonProperty(value = "user_id")
	private int userId;

	/**
	 * json: resource
	 */
	@JsonProperty(value = "resource")
	private String resource;

	/**
	 * json: topic
	 */
	@JsonProperty(value = "topic")
	private String topic;

	/**
	 * json: received
	 */
	@JsonProperty(value = "received")
	private String received;

	/**
	 * json: application_id
	 */
	@JsonProperty(value = "application_id")
	private int applicationId;

	/**
	 * json: sent
	 */
	@JsonProperty(value = "sent")
	private String sent;

	/**
	 * json: attempts
	 */
	@JsonProperty(value = "attempts")
	private int attempts;

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the resource
	 */
	public String getResource() {
		return resource;
	}

	/**
	 * @param resource
	 *            the resource to set
	 */
	public void setResource(String resource) {
		this.resource = resource;
	}

	/**
	 * @return the topic
	 */
	public String getTopic() {
		return topic;
	}

	/**
	 * @param topic
	 *            the topic to set
	 */
	public void setTopic(String topic) {
		this.topic = topic;
	}

	/**
	 * @return the received
	 */
	public String getReceived() {
		return received;
	}

	/**
	 * @param received
	 *            the received to set
	 */
	public void setReceived(String received) {
		this.received = received;
	}

	/**
	 * @return the applicationId
	 */
	public int getApplicationId() {
		return applicationId;
	}

	/**
	 * @param applicationId
	 *            the applicationId to set
	 */
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	/**
	 * @return the sent
	 */
	public String getSent() {
		return sent;
	}

	/**
	 * @param sent
	 *            the sent to set
	 */
	public void setSent(String sent) {
		this.sent = sent;
	}

	/**
	 * @return the attempts
	 */
	public int getAttempts() {
		return attempts;
	}

	/**
	 * @param attempts
	 *            the attempts to set
	 */
	public void setAttempts(int attempts) {
		this.attempts = attempts;
	}

	@Override
	public String toString() {
		return "Notification [userId=" + userId + ", " + (resource != null ? "resource=" + resource + ", " : "")
				+ (topic != null ? "topic=" + topic + ", " : "")
				+ (received != null ? "received=" + received + ", " : "") + "applicationId=" + applicationId + ", "
				+ (sent != null ? "sent=" + sent + ", " : "") + "attempts=" + attempts + "]";
	}
}

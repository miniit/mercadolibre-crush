package br.com.miniit.crush.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import javax.validation.constraints.Size;

public class Product implements Serializable {

	private static final long serialVersionUID = -7696553338275625505L;
	private String id;
	@Size(min = 3, max = 100)
	private String name;
	private int selectedPlataform;
	private Map<Integer, String> plataforms;
	private int selectedCategory;
	private Map<Integer, String> categories;
	private int selectedCountry;
	private Map<Integer, String> countries;
	private BigDecimal value;
	private int quantity;

	public BigDecimal getTotalValue() {
		return this.value.multiply(new BigDecimal(this.quantity));
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the value
	 */
	public BigDecimal getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(BigDecimal value) {
		this.value = value;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the selectedPlataform
	 */
	public int getSelectedPlataform() {
		return selectedPlataform;
	}

	/**
	 * @param selectedPlataform
	 *            the selectedPlataform to set
	 */
	public void setSelectedPlataform(int selectedPlataform) {
		this.selectedPlataform = selectedPlataform;
	}

	/**
	 * @return the plataforms
	 */
	public Map<Integer, String> getPlataforms() {
		return plataforms;
	}

	/**
	 * @param plataforms
	 *            the plataforms to set
	 */
	public void setPlataforms(Map<Integer, String> plataforms) {
		this.plataforms = plataforms;
	}

	/**
	 * @return the selectedCategory
	 */
	public int getSelectedCategory() {
		return selectedCategory;
	}

	/**
	 * @param selectedCategory
	 *            the selectedCategory to set
	 */
	public void setSelectedCategory(int selectedCategory) {
		this.selectedCategory = selectedCategory;
	}

	/**
	 * @return the categories
	 */
	public Map<Integer, String> getCategories() {
		return categories;
	}

	/**
	 * @param categories
	 *            the categories to set
	 */
	public void setCategories(Map<Integer, String> categories) {
		this.categories = categories;
	}

	/**
	 * @return the selectedCountry
	 */
	public int getSelectedCountry() {
		return selectedCountry;
	}

	/**
	 * @param selectedCountry
	 *            the selectedCountry to set
	 */
	public void setSelectedCountry(int selectedCountry) {
		this.selectedCountry = selectedCountry;
	}

	/**
	 * @return the countries
	 */
	public Map<Integer, String> getCountries() {
		return countries;
	}

	/**
	 * @param countries
	 *            the countries to set
	 */
	public void setCountries(Map<Integer, String> countries) {
		this.countries = countries;
	}
}
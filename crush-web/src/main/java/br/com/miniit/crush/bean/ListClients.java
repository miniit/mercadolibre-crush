package br.com.miniit.crush.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;

import br.com.miniit.crush.vo.Client;
import br.com.miniit.crush.vo.Order;

@Named
@ViewScoped
public class ListClients implements Serializable {

	private static final long serialVersionUID = -5451701575191675361L;
	@Inject
	private MessageBundle msg;
	private List<Client> clients;
	private String contentHeader;
	private int searchCriteria;
	private String searchText;

	@PostConstruct
	public void init() {
		contentHeader = msg.getString("listClients");
		clients = new ArrayList<>();
		Client client = null;
		Map<Integer, String> ufs = new LinkedHashMap<>();
		ufs.put(1, "SP");
		ufs.put(2, "RJ");
		for (int i = 1; i <= 11; i++) {
			client = new Client();
			client.setName("Rafael Or�gio");
			client.setNickname("rafaeloragio");
			client.setEmail("rafael.oragio@gmail.com");
			client.setPhone("(19) 99753-5827");
			client.setCity("Campinas");
			client.setUfs(ufs);
			client.setSelectedUf(1);
			client.setRegisterDate(new Date());
			Order order = null;
			for (int j = 1; j <= 11; j++) {
				order = new Order();
				order.setNumber(100000 + j);
				client.getOrders().add(order);
			}
			clients.add(client);
		}
	}

	public void edit(Client product) {
		System.out.println(product);
	}

	public void openOrder(Order order) {
		System.out.println(order);
	}
	
	public void search() {
		
	}

	/**
	 * @return the contentHeader
	 */
	public String getContentHeader() {
		return contentHeader;
	}

	/**
	 * @param contentHeader
	 *            the contentHeader to set
	 */
	public void setContentHeader(String contentHeader) {
		this.contentHeader = contentHeader;
	}

	/**
	 * @return the clients
	 */
	public List<Client> getClients() {
		return clients;
	}

	/**
	 * @param clients
	 *            the clients to set
	 */
	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	/**
	 * @return the searchCriteria
	 */
	public int getSearchCriteria() {
		return searchCriteria;
	}

	/**
	 * @param searchCriteria
	 *            the searchCriteria to set
	 */
	public void setSearchCriteria(int searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	/**
	 * @return the searchText
	 */
	public String getSearchText() {
		return searchText;
	}

	/**
	 * @param searchText
	 *            the searchText to set
	 */
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
}
# About

The project goal is to integrate the MercadoLibre platform to a custom vending service.

# Technologies

* Java 8
* Java EE7
* EJB 3.1
* JSF2
* Primefaces 6
* Bootstrap
* Hibernate 5 + JPA
* Wildfly 10
* PostgreSQL 9.6

# MercadoLibre Dev

Development site of the MercadoLibre API: [http://developers.mercadolibre.com/](http://developers.mercadolibre.com/)

# MercadoLibre Java SDK

MercadoLibre API: [https://github.com/mercadolibre/java-sdk](https://github.com/mercadolibre/java-sdk)

# Wildfly + SSL

To receive notifications and enable OAuth authentication with the app, it's needed to setting up Wildfly with SSL: [https://docs.jboss.org/author/display/WFLY8/SSL+setup+guide](https://docs.jboss.org/author/display/WFLY8/SSL+setup+guide)

And we'll use [certbot](https://github.com/certbot/certbot) to create a valid SSL certificate.

# Project

Workana project: https://www.workana.com/job/software-de-envio-automatico-integrado-com-o-mercado-livre

## Project Specification

The functional documentation can be accessed by this [link (pdf)](docs/UCs.pdf).

# Developers

* [Ricardo Ichizo](@richizo) 
* [Rafael Orágio](@rafaeloragio)
package br.com.miniit.crush.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "order")
@Audited
public class Client implements Serializable {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private Long id;
	@Column(name = "name")
	private String name;
	@Column(name = "nickname")
	private String nickname;
	@Column(name = "email")
	private String email;
	@Column(name = "phone")
	private String phone;
	@Column(name = "city")
	private String city;
	@Column(name = "state")
	private String state;
	@Column(name = "creation_date")
	private Date creationDate;
	@OneToMany(orphanRemoval = true, fetch = FetchType.EAGER)
	private List<Order> orders;

}
package br.com.miniit.crush.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "product")
@Audited
public class Product implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column
	private String name;
	@Column
	private String plataform;
	@Column
	private String category;
	@Column
	private String country;
	@Column
	private BigDecimal value;
	@Column
	private int quantity;
}
package br.com.miniit.crush.service;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;

import com.mercadolibre.sdk.Meli;

import br.com.miniit.crush.util.Constants;
import br.com.miniit.crush.util.PropertiesUtil;

/**
 * Class responsible to communicate with the ML.
 */
@Singleton
public class MercadoLibre {

	/**
	 * ML API.
	 */
	private Meli mercadoLibreApi;

	/**
	 * Initialization.
	 */
	@PostConstruct
	public void init() {
		// read through the properties file
		String appId = PropertiesUtil.getInstance().getProp(Constants.ML_APP_ID);
		String appKey = PropertiesUtil.getInstance().getProp(Constants.ML_SECRET_KEY);

		mercadoLibreApi = new Meli(Long.valueOf(appId), appKey);
	}

	/**
	 * @return the mercadoLibreApi
	 */
	public Meli getMercadoLibreApi() {
		return mercadoLibreApi;
	}

	/**
	 * @param mercadoLibreApi
	 *            the mercadoLibreApi to set
	 */
	public void setMercadoLibreApi(Meli mercadoLibreApi) {
		this.mercadoLibreApi = mercadoLibreApi;
	}

}

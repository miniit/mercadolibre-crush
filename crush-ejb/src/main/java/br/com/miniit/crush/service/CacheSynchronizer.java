package br.com.miniit.crush.service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.apache.log4j.Logger;

import br.com.miniit.crush.util.Constants;
import br.com.miniit.crush.util.PropertiesUtil;

/**
 * TimerService to synchronize all the caches.
 * 
 * More information:
 * {@link http://developers.mercadolibre.com/pt-br/dump-de-categorias/}
 */
@Startup
@Singleton
public class CacheSynchronizer {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(CacheSynchronizer.class);

	/**
	 * Properties utilities.
	 */
	PropertiesUtil propUtil = PropertiesUtil.getInstance();

	/**
	 * Timer Service.
	 */
	@Resource
	private TimerService timerService;

	/**
	 * ML API.
	 */
	@EJB
	private MercadoLibre ml;

	/**
	 * Initialization.
	 */
	@PostConstruct
	public void init() {
		ScheduleExpression exp = new ScheduleExpression();
		TimerConfig config = new TimerConfig();

		// it's not persistent
		config.setPersistent(false);

		// mount the CRON expression
		exp.hour(getHour()).minute(getMinute()).second(getSecond()).dayOfMonth(getDayOfMonth())
				.dayOfWeek(getDayOfWeek());
		timerService.createCalendarTimer(exp, config);
	}

	/**
	 * Does the synchronization.
	 */
	@Timeout
	public void sync() {
		syncGames();

		syncL10n();
	}

	protected void syncGames() {

	}

	protected void syncL10n() {

	}

	/**
	 * Gets the synchronization day of week.
	 * 
	 * @return day of week
	 */
	private String getDayOfWeek() {
		return propUtil.getProp(Constants.SC_DOW);
	}

	/**
	 * Gets the synchronization day of month.
	 * 
	 * @return day of month
	 */
	private String getDayOfMonth() {
		return propUtil.getProp(Constants.SC_DOM);
	}

	/**
	 * Gets the synchronization second.
	 * 
	 * @return second
	 */
	private String getSecond() {
		return propUtil.getProp(Constants.SC_SECOND);
	}

	/**
	 * Gets the synchronization minute.
	 * 
	 * @return minute
	 */
	private String getMinute() {
		return propUtil.getProp(Constants.SC_MINUTE);
	}

	/**
	 * Gets the synchronization hour.
	 * 
	 * @return hour
	 */
	private String getHour() {
		return propUtil.getProp(Constants.SC_HOUR);
	}

}

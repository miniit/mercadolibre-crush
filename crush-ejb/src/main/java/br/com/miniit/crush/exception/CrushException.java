package br.com.miniit.crush.exception;

/**
 * Custom class for exceptions.
 */
public class CrushException extends Exception {

	/**
	 * serial uid.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 * 
	 * @param e
	 *            exception
	 */
	public CrushException(Exception e) {
		super(e);
	}

	/**
	 * Constructor.
	 * 
	 * @param msg
	 *            message
	 */
	public CrushException(String msg) {
		super(msg);
	}

	/**
	 * Constructor.
	 * 
	 * @param msg
	 *            message
	 * @param e
	 *            exception
	 */
	public CrushException(String msg, Exception e) {
		super(msg, e);
	}
}

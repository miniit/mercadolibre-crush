package br.com.miniit.crush.cache;

/**
 * Cache for States.
 */
public class StatesCache extends Cache {
	/**
	 * singleton instance.
	 */
	private static StatesCache instance = null;

	/**
	 * Private constructor for singleton access.
	 */
	private StatesCache() {
	}

	/**
	 * Singleton access.
	 * 
	 * @return the instance of StatesCache class
	 */
	public static synchronized StatesCache getInstance() {
		if (instance == null) {
			instance = new StatesCache();
		}

		return instance;
	}

}

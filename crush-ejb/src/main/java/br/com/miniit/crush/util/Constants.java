package br.com.miniit.crush.util;

/**
 * Constants.
 */
public final class Constants {

	/**
	 * Properties file.
	 */
	public static final String PROPERTIES_FILE = "crush.properties";

	/**
	 * Properties key: MERCADOLIBRE_APP_ID.
	 */
	public static final String ML_APP_ID = "MERCADOLIBRE_APP_ID";

	/**
	 * Properties key: MERCADOLIBRE_SECRET_KEY.
	 */
	public static final String ML_SECRET_KEY = "MERCADOLIBRE_SECRET_KEY";

	/**
	 * Properties key: MAILGUN_API_KEY.
	 */
	public static final String MG_API_KEY = "MAILGUN_API_KEY";

	/**
	 * Properties key: MAILGUN_DOMAIN_NAME.
	 */
	public static final String MG_DOMAIN = "MAILGUN_DOMAIN_NAME";

	/**
	 * Properties key: USER_CONFIGURATION_EMAIL_CONTACT.
	 */
	public static final String UC_EMAIL_CONTACT = "USER_CONFIGURATION_EMAIL_CONTACT";

	/**
	 * Properties key: USER_CONFIGURATION_LOW_STOCK.
	 */
	public static final String UC_LOW_STOCK = "USER_CONFIGURATION_LOW_STOCK";

	/**
	 * Properties key: SYNC_CACHE_HOUR.
	 */
	public static final String SC_HOUR = "SYNC_CACHE_HOUR";

	/**
	 * Properties key: SYNC_CACHE_MINUTE.
	 */
	public static final String SC_MINUTE = "SYNC_CACHE_MINUTE";

	/**
	 * Properties key: SYNC_CACHE_SECOND.
	 */
	public static final String SC_SECOND = "SYNC_CACHE_SECOND";

	/**
	 * Properties key: SYNC_CACHE_DAY_OF_MONTH.
	 */
	public static final String SC_DOM = "SYNC_CACHE_DAY_OF_MONTH";

	/**
	 * Properties key: SYNC_CACHE_DAY_OF_MONTH.
	 */
	public static final String SC_DOW = "SYNC_CACHE_DAY_OF_WEEK";

	/**
	 * Enum to Mailgun subject types.
	 */
	public enum Subject {
		ITEM("[MercadoLibre - Crush] - Item"), ORDER("[MercadoLibre - Crush] - Order");

		private String subject;

		Subject(String subject) {
			this.subject = subject;
		}

		@Override
		public String toString() {
			return subject;
		}
	}
	/**
	 * Private constructor.
	 */
	private Constants() {
	}
}

package br.com.miniit.crush.service;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import br.com.miniit.crush.util.Constants;
import br.com.miniit.crush.util.Constants.Subject;
import br.com.miniit.crush.util.PropertiesUtil;

/**
 * Send mails through the Mailgun service API.
 * 
 * More information:
 * {@link https://documentation.mailgun.com/api-sending.html#examples}
 */
@Singleton
public class SendMail {

	/**
	 * Mailgun API URL.
	 */
	private static final String MAILGUN_API = "https://api.mailgun.net/v3/";

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(SendMail.class);

	/**
	 * Properties utilities.
	 */
	private PropertiesUtil propUtil = PropertiesUtil.getInstance();

	/**
	 * WS client.
	 */
	Client client;

	/**
	 * Initialization.
	 */
	@PostConstruct
	public void init() {
		client = ClientBuilder.newClient();
	}

	/**
	 * Gets the default Mailgun URL.
	 * 
	 * @return Mailgun URL API
	 */
	private String getUrl() {
		return MAILGUN_API + propUtil.getProp(Constants.MG_DOMAIN);
	}

	/**
	 * Gets the default mail sender.
	 * 
	 * @return noreply@yourdomain
	 */
	private String getSender() {
		return "noreply@" + propUtil.getProp(Constants.MG_DOMAIN);
	}

	/**
	 * Gets the default mail receiver.
	 * 
	 * @return the receiver configured on the properties file
	 */
	private String getReceiver() {
		return propUtil.getProp(Constants.UC_EMAIL_CONTACT);
	}

	/**
	 * Send a mail message to the default contact.
	 * 
	 * @param subject
	 * @param message
	 */
	public void sendMessage(Subject subject, String message) {
		sendMessage(subject, message, getReceiver());
	}

	/**
	 * Send a mail message to a contact.
	 * 
	 * @param subject
	 * @param message
	 * @param receiver
	 */
	public void sendMessage(Subject subject, String message, String receiver) {
		// mounts the URL
		WebTarget target = client.target(getUrl());
		target.path("messages");

		// adds the query params
		target.queryParam("from", getSender()).queryParam("to", receiver).queryParam("subject", subject.toString())
				.queryParam("text", message);

		// consume the web service
		target.request(MediaType.APPLICATION_JSON).async();
	}
}

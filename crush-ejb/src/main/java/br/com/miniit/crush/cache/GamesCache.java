package br.com.miniit.crush.cache;

/**
 * Games category cache.
 */
public class GamesCache extends Cache {

	/**
	 * singleton instance.
	 */
	private static GamesCache instance = null;

	/**
	 * Private constructor for singleton access.
	 */
	private GamesCache() {
	}

	/**
	 * Singleton access.
	 * 
	 * @return the instance of GamesCache class
	 */
	public static synchronized GamesCache getInstance() {
		if (instance == null) {
			instance = new GamesCache();
		}

		return instance;
	}

}

package br.com.miniit.crush.cache;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public abstract class Cache {

	/**
	 * cache.
	 */
	private Map<String, String> cacheMap = new ConcurrentHashMap<>();

	/**
	 * Clear the cache.
	 */
	public void clearCache() {
		cacheMap.clear();
	}

	/**
	 * Adds a {key,value} to the cache.
	 * 
	 * @param key
	 * @param value
	 */
	public void addToCache(String key, String value) {
		cacheMap.put(key, value);
	}

	/**
	 * Gets the value related to the cache key.
	 * 
	 * @param key
	 * @return value
	 */
	public String getCacheValue(String key) {
		return cacheMap.get(key);
	}

	/**
	 * Gets all the cache values.
	 * 
	 * @return collection of values
	 */
	public Collection<String> getCacheValues() {
		return cacheMap.values();
	}

	/**
	 * Gets all the cache keys.
	 * 
	 * @return set of keys
	 */
	public Set<String> getCacheKeys() {
		return cacheMap.keySet();
	}

	/**
	 * Verifies if the cache contains a key.
	 * 
	 * @param key
	 * @return value
	 */
	public boolean containsKey(String key) {
		return cacheMap.containsKey(key);
	}
}

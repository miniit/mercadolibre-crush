package br.com.miniit.crush.cache;

/**
 * Cache for Cities.
 */
public class CitiesCache extends Cache {
	/**
	 * singleton instance.
	 */
	private static CitiesCache instance = null;

	/**
	 * Private constructor for singleton access.
	 */
	private CitiesCache() {
	}

	/**
	 * Singleton access.
	 * 
	 * @return the instance of CitiesCache class
	 */
	public static synchronized CitiesCache getInstance() {
		if (instance == null) {
			instance = new CitiesCache();
		}

		return instance;
	}

}

package br.com.miniit.crush.service;

import javax.ejb.Stateless;

@Stateless
public class ServiceImpl {
	
	public int calculate(int a, int b) {
		return a + b;
	}
}

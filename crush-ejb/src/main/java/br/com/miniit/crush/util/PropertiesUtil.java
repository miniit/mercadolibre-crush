package br.com.miniit.crush.util;

import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Properties utilities.
 */
public final class PropertiesUtil {

	/**
	 * logger.
	 */
	private static final Logger logger = Logger.getLogger(PropertiesUtil.class);

	/**
	 * singleton instance.
	 */
	private static PropertiesUtil instance = null;

	/**
	 * java properties.
	 */
	private static Properties p = null;

	/**
	 * Private constructor for singleton access.
	 */
	private PropertiesUtil() {
	}

	/**
	 * Singleton access.
	 * 
	 * @return the instance of PropertiesUtil class
	 */
	public static synchronized PropertiesUtil getInstance() {
		if (instance == null) {
			instance = new PropertiesUtil();
			p = new Properties();

			try {
				// read the properties file
				InputStream inStream = PropertiesUtil.class.getClassLoader()
						.getResourceAsStream(Constants.PROPERTIES_FILE);
				p.load(inStream);

			} catch (Exception e) {
				logger.error("Error while reading the properties file", e);
			}
		}

		return instance;
	}

	/**
	 * Gets the property value.
	 * 
	 * @param key
	 * @return value
	 */
	public String getProp(String key) {
		return p.getProperty(key);
	}
}

package br.com.miniit.crush.cache;

/**
 * Cache for Countries.
 */
public class CountriesCache extends Cache {
	/**
	 * singleton instance.
	 */
	private static CountriesCache instance = null;

	/**
	 * Private constructor for singleton access.
	 */
	private CountriesCache() {
	}

	/**
	 * Singleton access.
	 * 
	 * @return the instance of CountriesCache class
	 */
	public static synchronized CountriesCache getInstance() {
		if (instance == null) {
			instance = new CountriesCache();
		}

		return instance;
	}

}
